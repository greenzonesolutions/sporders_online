﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;


namespace SpordersOnline
{
    public partial class _Speisekarte : Page
    {
        String restaurantname;
        string sfontcolor = "black";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Parameter = restaurantname auslesen | versch. Parameter zulassen
            if (Request.QueryString["restaurantname"] != null)
            {
                restaurantname = Request.QueryString["restaurantname"];
            }
            if (Request.QueryString["firma"] != null)
            {
                restaurantname = Request.QueryString["firma"];
            }
            if (Request.QueryString["restaurant"] != null)
            {
                restaurantname = Request.QueryString["restaurant"];
            }
            if (!Page.IsPostBack)//nur beim ersten Laden
            {
              
                try
                {
                    //Bilder aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getPictureUrl.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string bildurl = data["bildurl0"].Value<string>();
                        string backgroundurl = data["backgroundurl0"].Value<string>();
                        sfontcolor = data["fontcolor0"].Value<string>();
                        Image1.ImageUrl = bildurl.TrimEnd('\r', '\n');//warum auch immer werden an das Ende des String Linefeeds hinzugefügt, die lösche ich hier wieder weg
                        divMasthead.Style["background-image"] = Page.ResolveUrl(backgroundurl);
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }


                
                try
                {
                    //Leistungsmerkmale aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantLeistungsmerkmale.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string leistungsmerkmale = data["leistungsmerkmale0"].Value<string>();

                        if (leistungsmerkmale.Contains("liefern") || leistungsmerkmale.Contains("bestellen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_bestellen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservieren"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservieren").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservierungsanfragen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservierungsanfragen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("registrierungspflicht"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_registrierungspflicht").Visible = false;
                        }
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
                
                try
                {
                    //Speisekarte holen und anzeigen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getSpeisekarte.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string speisekarte = data["speisekarte0"].Value<string>();
                        divSpeisekarte.InnerHtml = speisekarte;
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }

                lbHeadline.Text = "Willkommen bei " + restaurantname;
            }



            //Textfarbe anpassen
            System.Drawing.Color fontcolor = System.Drawing.Color.Black;
            //change Color
            if (sfontcolor.Equals("white"))
            {
                fontcolor = System.Drawing.Color.White;
            }
            if (sfontcolor.Equals("black"))
            {
                fontcolor = System.Drawing.Color.Black;
            }
            lbHeadline.ForeColor = fontcolor;
            divSpeisekarte.Style.Add("color", "black");


        }

       
    }
}