﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpordersOnline
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string restaurantname;
            if (!Page.IsPostBack)//nur beim ersten Laden
            {
                try
                {
                    restaurantname = Request.QueryString["restaurantname"];

                    //Leistungsmerkmale aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantLeistungsmerkmale.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string leistungsmerkmale = data["leistungsmerkmale0"].Value<string>();
                        if (leistungsmerkmale.Contains("liefern") || leistungsmerkmale.Contains("bestellen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_bestellen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservieren"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservieren").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservierungsanfragen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservierungsanfragen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("registrierungspflicht"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_registrierungspflicht").Visible = false;
                        }
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}