﻿<%@ Page Title="Impressum" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Impressum.aspx.cs" Inherits="SpordersOnline.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p class="MsoNormal" style="color: #FFFFFF">
        <b><span>Impressum<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Informationspflicht laut § 5 TMG.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Greenzone UG (haftungsbeschränkt)<br />
    Im Wasserstein 1,
    <br />
    78073 Bad Dürrheim,
    <br />
    Deutschland<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Register:</span></b><span> Handelsregister<br />
    <b>Registernummer:</b> HRB 716050<br />
    <b>Registergericht:</b> Freiburg<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Tel.:</span></b><span> 017670835072<br />
    <b>E-Mail:</b> <a href="mailto:info@greenzone-solutions.com">info@greenzone-solutions.com</a><o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Berufsbezeichnung:</span></b><span> Softwareentwicklung<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Geschäftsführer</span></b><span><br />
    Markus Barho<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>EU-Streitschlichtung<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Gemäß Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-Verordnung) möchten wir Sie über die Online-Streitbeilegungsplattform (OS-Plattform) informieren.<br />
    Verbraucher haben die Möglichkeit, Beschwerden an die Online Streitbeilegungsplattform der Europäischen Kommission unter <a href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&amp;lng=DE" target="_blank">http://ec.europa.eu/odr?tid=321128889</a> zu richten. Die dafür notwendigen Kontaktdaten finden Sie oberhalb in unserem Impressum.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wir möchten Sie jedoch darauf hinweisen, dass wir nicht bereit oder verpflichtet sind, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Haftung für Inhalte dieser Website<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wir entwickeln die Inhalte dieser Webseite ständig weiter und bemühen uns korrekte und aktuelle Informationen bereitzustellen. Laut Telemediengesetz <a href="https://www.gesetze-im-internet.de/tmg/__7.html?tid=321128889" target="_blank">(TMG) §7 (1)</a> sind wir als Diensteanbieter für eigene Informationen, die wir zur Nutzung bereitstellen, nach den allgemeinen Gesetzen verantwortlich. Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Webseite übernehmen, speziell für jene die seitens Dritter bereitgestellt wurden. Als Diensteanbieter im Sinne der §§ 8 bis 10 sind wir nicht verpflichtet, die von ihnen übermittelten oder gespeicherten Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Unsere Verpflichtungen zur Entfernung von Informationen oder zur Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen aufgrund von gerichtlichen oder behördlichen Anordnungen bleiben auch im Falle unserer Nichtverantwortlichkeit nach den §§ 8 bis 10 unberührt. <o:p></o:p></span>
</p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitte wir Sie uns umgehend zu kontaktieren, damit wir die rechtswidrigen Inhalte entfernen können. Sie finden die Kontaktdaten im Impressum.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Haftung für Links auf dieser Website und in App</span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Unsere Webseite enthält Links zu anderen Webseiten für deren Inhalt wir nicht verantwortlich sind. Haftung für verlinkte Websites besteht für uns nicht, da wir keine Kenntnis rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und wir Links sofort entfernen würden, wenn uns Rechtswidrigkeiten bekannt werden.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitte wir Sie uns zu kontaktieren. Sie finden die Kontaktdaten im Impressum.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Urheberrechtshinweis<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Alle Inhalte dieser Webseite (Bilder, Fotos, Texte, Videos) unterliegen dem Urheberrecht der Bundesrepublik Deutschland. Bitte fragen Sie uns bevor Sie die Inhalte dieser Website verbreiten, vervielfältigen oder verwerten wie zum Beispiel auf anderen Websites erneut veröffentlichen. Falls notwendig, werden wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Sollten Sie auf dieser Webseite Inhalte finden, die das Urheberrecht verletzen, bitten wir Sie uns zu kontaktieren.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Bildernachweis<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Die Bilder, Fotos und Grafiken auf dieser Webseite sind urheberrechtlich geschützt.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Die Bilderrechte liegen bei den folgenden Fotografen und Unternehmen:<o:p></o:p></span></p>
<ul type="disc">
    <li class="MsoNormal"><span>Fotograf Mustermann<o:p></o:p></span></li>
</ul>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Datenschutzerklärung<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Datenschutz<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wir haben diese Datenschutzerklärung (Fassung 17.11.2019-321128889) verfasst, um Ihnen gemäß der Vorgaben der <a href="https://eur-lex.europa.eu/legal-content/DE/ALL/?tid=1234&amp;uri=celex%3A32016R0679&amp;tid=321128889" target="_blank">Datenschutz-Grundverordnung (EU) 2016/679</a> zu erklären, welche Informationen wir sammeln, wie wir Daten verwenden und welche Entscheidungsmöglichkeiten Sie als Besucher dieser Webseite haben.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Leider liegt es in der Natur der Sache, dass diese Erklärungen sehr technisch klingen, wir haben uns bei der Erstellung jedoch bemüht die wichtigsten Dinge so einfach und klar wie möglich zu beschreiben.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Automatische Datenspeicherung<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wenn Sie heutzutage Webseiten besuchen, werden gewisse Informationen automatisch erstellt und gespeichert, so auch auf dieser Webseite.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wenn Sie unsere Webseite so wie jetzt gerade besuchen, speichert unser Webserver (Computer auf dem diese Webseite gespeichert ist) automatisch Daten wie<o:p></o:p></span></p>
<ul type="disc">
    <li class="MsoNormal" style="color: #FFFFFF"><span>die Adresse (URL) der aufgerufenen Webseite<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Browser und Browserversion<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>das verwendete Betriebssystem<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>die Adresse (URL) der zuvor besuchten Seite (Referrer URL)<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>den Hostname und die IP-Adresse des Geräts von welchem aus zugegriffen wird<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Datum und Uhrzeit<o:p></o:p></span></li>
</ul>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>in Dateien (Webserver-Logfiles).<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>In der Regel werden Webserver-Logfiles zwei Wochen gespeichert und danach automatisch gelöscht. Wir geben diese Daten nicht weiter, können jedoch nicht ausschließen, dass diese Daten beim Vorliegen von rechtswidrigem Verhalten eingesehen werden.<br />
    Die Rechtsgrundlage besteht nach <a href="https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016R0679&amp;from=DE&amp;tid=321128889" target="_blank">Artikel 6&nbsp; Absatz 1 f DSGVO</a> (Rechtmäßigkeit der Verarbeitung) darin, dass berechtigtes Interesse daran besteht, den fehlerfreien Betrieb dieser Webseite und der App durch das Erfassen von Webserver-Logfiles zu ermöglichen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Cookies<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Unsere Website verwendet HTTP-Cookies um nutzerspezifische Daten zu speichern.<br />
    Im Folgenden erklären wir, was Cookies sind und warum Sie genutzt werden, damit Sie die folgende Datenschutzerklärung besser verstehen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Was genau sind Cookies?<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Immer wenn Sie durch das Internet surfen, verwenden Sie einen Browser. Bekannte Browser sind beispielsweise Chrome, Safari, Firefox, Internet Explorer und Microsoft Edge. Die meisten Webseiten speichern kleine Text-Dateien in Ihrem Browser. Diese Dateien nennt man Cookies.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Eines ist nicht von der Hand zu weisen: Cookies sind echt nützliche Helferlein. Fast alle Webseiten verwenden&nbsp;Cookies. Genauer gesprochen sind es HTTP-Cookies, da es auch noch andere&nbsp;Cookies für andere Anwendungsbereiche gibt. HTTP-Cookies&nbsp;sind kleine Dateien, die von unserer Website auf Ihrem Computer gespeichert werden. Diese Cookie-Dateien werden automatisch im Cookie-Ordner, quasi dem “Hirn” Ihres Browsers, untergebracht. Ein Cookie besteht aus einem Namen und einem Wert. Bei der Definition eines Cookies müssen zusätzlich ein oder mehrere Attribute angegeben werden.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Cookies&nbsp;speichern gewisse Nutzerdaten von Ihnen, wie beispielsweise Sprache oder persönliche Seiteneinstellungen. Wenn Sie unsere Seite wieder aufrufen, übermittelt Ihr Browser die „userbezogenen“ Informationen an unsere Seite zurück. Dank der Cookies weiß unsere Website, wer Sie sind und bietet Ihnen Ihre gewohnte Standardeinstellung. In einigen Browsern hat jedes&nbsp;Cookie&nbsp;eine eigene Datei, in anderen wie beispielsweise Firefox sind alle&nbsp;Cookies&nbsp;in einer einzigen Datei gespeichert.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Es gibt sowohl Erstanbieter Cookies als auch Drittanbieter-Cookies. Erstanbieter-Cookies werden direkt von unserer Seite erstellt, Drittanbieter-Cookies werden von Partner-Webseiten (z.B. Google Analytics) erstellt.&nbsp;Jedes Cookie ist individuell zu bewerten, da jedes Cookie andere Daten speichert. Auch die Ablaufzeit eines Cookies variiert von ein paar Minuten bis hin zu ein paar Jahren.&nbsp;Cookies sind keine Software-Programme und enthalten keine Viren, Trojaner oder andere „Schädlinge“. Cookies können auch nicht auf Informationen Ihres PCs zugreifen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>So können zum Beispiel Cookie-Daten aussehen:<o:p></o:p></span></p>
<ul type="disc">
    <li class="MsoNormal" style="color: #FFFFFF"><span>Name: _ga<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Ablaufzeit: 2 Jahre<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Verwendung: Unterscheidung der Webseitenbesucher<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Beispielhafter Wert:&nbsp;GA1.2.1326744211.152321128889<o:p></o:p></span></li>
</ul>
<p class="MsoNormal">
    <span>Ein Browser sollte folgende Mindestgrößen unterstützen:<o:p></o:p></span></p>
<ul type="disc">
    <li class="MsoNormal" style="color: #FFFFFF"><span>Ein Cookie soll mindestens 4096 Bytes enthalten können<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Pro Domain sollen mindestens 50 Cookies gespeichert werden können<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Insgesamt sollen mindestens 3000 Cookies gespeichert werden können<o:p></o:p></span></li>
</ul>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Welche Arten von Cookies gibt es?<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Die Frage welche Cookies wir im Speziellen verwenden, hängt von den verwendeten Diensten ab und wird in der folgenden Abschnitten der Datenschutzerklärung geklärt. An dieser Stelle möchten wir kurz auf die verschiedenen Arten von HTTP-Cookies eingehen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Man kann 4 Arten von Cookies unterscheiden:<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Unbedingt notwendige Cookies<br />
    </span></b><span>Diese Cookies sind nötig, um grundlegende Funktionen der Website sicherzustellen. Zum Beispiel braucht es diese Cookies, wenn ein User ein Produkt in den Warenkorb legt, dann auf anderen Seiten weitersurft und später erst zur Kasse geht. Durch diese Cookies wird der Warenkorb nicht gelöscht, selbst wenn der User sein Browserfenster schließt.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Funktionelle Cookies<br />
    </span></b><span>Diese Cookies sammeln Infos über das Userverhalten und ob der User etwaige Fehlermeldungen bekommt. Zudem werden mithilfe dieser Cookies auch die Ladezeit und das Verhalten der Website bei verschiedenen Browsern gemessen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Zielorientierte Cookies<br />
    </span></b><span style="color: #FFFFFF">Diese Cookies sorgen für eine bessere Nutzerfreundlichkeit. Beispielsweise werden eingegebene Standorte, Schriftgrößen oder Formulardaten gespeichert.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Werbe-Cookies<br />
    </span></b><span>Diese Cookies werden auch Targeting-Cookies genannt. Sie dienen dazu dem User individuell angepasste Werbung zu liefern. Das kann sehr praktisch, aber auch sehr nervig sein.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Üblicherweise werden Sie beim erstmaligen Besuch einer Webseite gefragt, welche dieser Cookiearten Sie zulassen möchten. Und natürlich wird diese Entscheidung auch in einem Cookie gespeichert.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Wie kann ich Cookies löschen?<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wie und ob Sie Cookies verwenden wollen, entscheiden Sie selbst. Unabhängig von welchem Service oder welcher Website die Cookies stammen, haben Sie immer die Möglichkeit&nbsp;Cookies zu löschen, nur teilweise zuzulassen oder zu deaktivieren. Zum Beispiel können Sie Cookies von Drittanbietern blockieren, aber alle anderen Cookies zulassen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wenn Sie feststellen möchten, welche Cookies in Ihrem Browser gespeichert wurden, wenn Sie Cookie-Einstellungen ändern oder löschen wollen, können Sie dies in Ihren Browser-Einstellungen finden:<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span><a href="https://support.google.com/chrome/answer/95647?tid=321128889" target="_blank">Chrome: Cookies in Chrome löschen, aktivieren und verwalten</a><o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span><a href="https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=321128889" target="_blank">Safari: Verwalten von Cookies und Websitedaten mit Safari</a><o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span><a href="https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=321128889" target="_blank">Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben</a><o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span><a href="https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=321128889" target="_blank">Internet Explorer: Löschen und Verwalten von Cookies</a><o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span><a href="https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=321128889" target="_blank">Microsoft Edge: Löschen und Verwalten von Cookies</a><o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Falls Sie grundsätzlich keine Cookies haben wollen, können Sie Ihren Browser so einrichten, dass er Sie immer informiert, wenn ein Cookie gesetzt werden soll. So können Sie bei jedem einzelnen Cookie entscheiden, ob Sie das Cookie erlauben oder nicht. Die Vorgangsweise ist je nach Browser verschieden. Am besten ist es Sie suchen die Anleitung in Google mit dem Suchbegriff “Cookies löschen Chrome” oder “Cookies deaktivieren Chrome” im Falle eines Chrome Browsers oder tauschen das Wort “Chrome” gegen den Namen Ihres Browsers, z.B. Edge, Firefox, Safari aus.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Wie sieht es mit meinem Datenschutz aus?<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Seit 2009 gibt es die sogenannten „Cookie-Richtlinien“. Darin ist festgehalten, dass das Speichern von Cookies eine Einwilligung des Website-Besuchers (also von Ihnen) verlangt. Innerhalb der EU-Länder gibt es allerdings noch sehr unterschiedliche Reaktionen auf diese Richtlinien. In Deutschland wurden die Cookie-Richtlinien nicht als nationales Recht umgesetzt. Stattdessen erfolgte die Umsetzung dieser Richtlinie weitgehend in § 15 Abs.3 des Telemediengesetzes (TMG).<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wenn Sie mehr über Cookies wissen möchten und vor technischen Dokumentationen nicht zurückscheuen, empfehlen wir&nbsp;<a href="https://tools.ietf.org/html/rfc6265" target="_blank">https://tools.ietf.org/html/rfc6265</a>, dem Request for Comments der Internet Engineering Task Force (IETF) namens “HTTP State Management Mechanism”.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Speicherung persönlicher Daten<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Persönliche Daten, die Sie uns auf dieser Website oder in der App elektronisch übermitteln, wie zum Beispiel Name, E-Mail-Adresse, Adresse oder andere persönlichen Angaben im Rahmen der Übermittlung eines Formulars oder Kommentaren im Blog, werden von uns gemeinsam mit dem Zeitpunkt und der IP-Adresse nur zum jeweils angegebenen Zweck verwendet, sicher verwahrt und nicht an Dritte weitergegeben.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wir nutzen Ihre persönlichen Daten somit nur für die Kommunikation mit jenen Besuchern, die Kontakt ausdrücklich wünschen und für die Abwicklung der auf dieser Webseite oder in der App angebotenen Dienstleistungen und Produkte. Wir geben Ihre persönlichen Daten ohne Zustimmung nicht weiter, können jedoch nicht ausschließen, dass diese Daten beim Vorliegen von rechtswidrigem Verhalten eingesehen werden.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wenn Sie uns persönliche Daten per E-Mail schicken – somit abseits dieser Webseite – können wir keine sichere Übertragung und den Schutz Ihrer Daten garantieren. Wir empfehlen Ihnen, vertrauliche Daten niemals unverschlüsselt per E-Mail zu übermitteln.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Die Rechtsgrundlage besteht nach <a href="https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016R0679&amp;from=DE&amp;tid=321128889" target="_blank">Artikel 6&nbsp; Absatz 1 a DSGVO</a> (Rechtmäßigkeit der Verarbeitung) darin, dass Sie uns die Einwilligung zur Verarbeitung der von Ihnen eingegebenen Daten geben. Sie können diesen Einwilligung jederzeit widerrufen – eine formlose E-Mail reicht aus, Sie finden unsere Kontaktdaten im Impressum.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Rechte laut Datenschutzgrundverordnung<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Ihnen stehen laut den Bestimmungen der DSGVO grundsätzlich die folgende Rechte zu:<o:p></o:p></span></p>
<ul type="disc">
    <li class="MsoNormal" style="color: #FFFFFF"><span>Recht auf Berichtigung (Artikel 16 DSGVO)<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Recht auf Löschung („Recht auf Vergessenwerden“) (Artikel 17 DSGVO)<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Recht auf Einschränkung der Verarbeitung (Artikel 18 DSGVO)<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Recht auf Benachrichtigung – Mitteilungspflicht im Zusammenhang mit der Berichtigung oder Löschung personenbezogener Daten oder der Einschränkung der Verarbeitung (Artikel 19 DSGVO)<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Recht auf Datenübertragbarkeit (Artikel 20 DSGVO)<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Widerspruchsrecht (Artikel 21 DSGVO)<o:p></o:p></span></li>
    <li class="MsoNormal" style="color: #FFFFFF"><span>Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung — einschließlich Profiling — beruhenden Entscheidung unterworfen zu werden (Artikel 22 DSGVO)<o:p></o:p></span></li>
</ul>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Wenn Sie glauben, dass die Verarbeitung Ihrer Daten gegen das Datenschutzrecht verstößt oder Ihre datenschutzrechtlichen Ansprüche sonst in einer Weise verletzt worden sind, können Sie sich an die <a href="https://www.bfdi.bund.de/DE/Datenschutz/Ueberblick/MeineRechte/Artikel/BeschwerdeBeiDatenschutzbehoereden.html?tid=321128889" target="_blank">Bundesbeauftragte für den Datenschutz und die Informationsfreiheit (BfDI)</a> wenden.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Auswertung des Besucherverhaltens<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span style="color: #FFFFFF">In der folgenden Datenschutzerklärung informieren wir Sie darüber, ob und wie wir Daten Ihres Besuchs dieser Website und in der App auswerten. Die Auswertung der gesammelten Daten erfolgt in der Regel anonym und wir können von Ihrem Verhalten auf dieser Website nicht auf Ihre Person schließen.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Mehr über Möglichkeiten dieser Auswertung der Besuchsdaten zu widersprechen erfahren Sie in der folgenden Datenschutzerklärung.<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span><o:p>&nbsp;</o:p></span></p>
    <address style="color: #FFFFFF">
        &nbsp;</address>
</asp:Content>
