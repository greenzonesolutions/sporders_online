﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SpordersOnline.Startup))]
namespace SpordersOnline
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
