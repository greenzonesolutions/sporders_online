﻿<%@ Page Title="Bestellen" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Bestellen.aspx.cs" Inherits="SpordersOnline._Bestellen" MaintainScrollPositionOnPostBack="true" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

     <div id="divMasthead" class="row" runat=server style="background-size: cover">
        <div class="col-md-4">
            <p>
                &nbsp;
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                <asp:Label ID="lbHeadline" runat="server" Font-Bold="True" Font-Size="XX-Large" ForeColor="Black" Text="Herzlich Willkommen bei "></asp:Label>
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                <asp:Image ID="Image1" runat="server" Width="100%" />
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                <asp:Label ID="Label8" runat="server" Text="Bitte geben Sie Ihre Bestellung ein:" Font-Bold="True" ForeColor="Black"></asp:Label>
            </p>
            <p>
                <asp:RadioButton ID="rbBestellen" runat="server" ForeColor="Black" GroupName="abholenliefern" Text="selber abholen" Visible="False" OnCheckedChanged="rbAbholenLiefernChanged" AutoPostBack="true" Checked="true" />
                <asp:RadioButton ID="rbLiefern" runat="server" ForeColor="Black" GroupName="abholenliefern" Text="liefern lassen" Visible="False" OnCheckedChanged="rbAbholenLiefernChanged" AutoPostBack="true" Checked="false" />
            </p>
            <table style="width: 70%; height: 380px;">
                <tr>
                    <td>
                        <asp:TextBox ID="tbTelefonnummer" runat="server" placeholder="Telefonnummer" Width="150px"></asp:TextBox>
                    &nbsp;
                        <asp:DropDownList ID="ddUhrzeit" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td style="height: 24px; width: 168px">
                        &nbsp;</td>
                </tr>
                <tr id="trAdresse" visible="False" runat="server">
                    <td style="height: 10px; ">
                        <asp:TextBox ID="tbAdresse" runat="server" placeholder="Adresse" Width="150px" Height="62px" Rows="5"></asp:TextBox>
                    </td>
                    <td style="height: 10px; width: 168px"></td>
                </tr>
                <tr>
                    <td style="height: 24px;">&nbsp;</td>
                    <td style="width: 168px; height: 24px;">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Auswahl" Font-Bold="True" ForeColor="Black"></asp:Label>
                        <br />
                        <asp:Panel ID="panel1" runat="server" Height="200px" Width="327px" ScrollBars="Horizontal">
                        <asp:GridView ID="gvProdukte" runat="server"
                            AutoGenerateColumns="False" Font-Names="Arial" ShowHeader="False"
                            Font-Size="11pt" BackColor="White" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="gvProdukte_SelectedIndexChanged" ScrollBars="Vertical" VerticalScrollbarMode="Visible" Height="120px" Width="316px">
                            <Columns>
                                <asp:BoundField ItemStyle-Width="150px" DataField="Name">
                                    <ItemStyle Width="150px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ItemStyle-Width="150px" DataField="Preis">
                                    <ItemStyle Width="150px"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                            <SelectedRowStyle BackColor="#3399FF" />
                        </asp:GridView>
                      </asp:Panel> 
                    </td>
                    <td style="width: 168px; height: 116px;">&nbsp;&nbsp;&nbsp;&nbsp;
                        <br />
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="height: 41px">
                        <br />
                <asp:Label ID="Label10" runat="server" Text="Anzahl:" Font-Bold="True" ForeColor="Black"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddAnzahl" runat="server">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                </asp:DropDownList>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btUebernehmen" runat="server" Text="In den Warenkorb" OnClick="btUebernehmen_Click" Height="26px" Width="170px" Font-Bold="True"/>
                    </td>
                    <td style="width: 168px; height: 41px;">
                    </td>
                </tr>
                </table>
            <p>
                &nbsp;
            </p>
            <p>
                <table style="width: 77%; height: 96px;">
                    <tr>
                        <td style="height: 24px; width: 157px">
                            <asp:Label ID="lbWarenkorb" runat="server" Text="Mein Warenkorb" Font-Bold="True" ForeColor="Black" Visible="False"></asp:Label>
                        </td>
                        <td style="height: 24px; width: 284px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 10px; width: 157px">
                            <asp:TextBox ID="tbWarenkorb" TextMode="MultiLine" runat="server" Enabled="False" Height="125px" Rows="8" Width="310px" Style="overflow: scroll" BackColor="White" Visible="False" ForeColor="Black"></asp:TextBox>
                        </td>
                        <td style="height: 10px; width: 284px"></td>
                    </tr>
                    <tr>
                        <td style="width: 157px; height: 24px;">
                            <asp:Label ID="lbGesamtpreisText" runat="server" Text="Gesamtpreis: " Font-Bold="True" ForeColor="Black" Visible="False"></asp:Label>
                            <asp:Label ID="lbGesamtpreis" runat="server" Text="00,00" Font-Bold="True" ForeColor="Black" Visible="False"></asp:Label>
                            <asp:Label ID="lbEuro" runat="server" Text="€" Font-Bold="True" ForeColor="Black" Visible="False"></asp:Label>
                        </td>
                        <td >
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 157px; height: 24px;">
                            <asp:Button ID="btZuruecksetzen" runat="server" Text="Warenkorb löschen" OnClick="btZuruecksetzen_Click" Width="170px" Visible="False" Font-Bold="True" />
                        </td>
                        <td >
                            &nbsp;</td>
                    </tr>
                </table>
            </p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="lbErrorMessage" runat="server" ForeColor="#CC0000" Text="Bitte füllen Sie alle Daten vollständig aus" Visible="False"></asp:Label>
            </p>
            <p>
                &nbsp;<asp:CheckBox ID="cbAGB" runat="server" Visible="False" />
                <asp:HyperLink ID="hlAGBs" runat="server" NavigateUrl="~/AGBs.aspx" Visible="False">Hiermit bestätige ich die AGBs und Datenschutzbestimmungen gelesen zu haben und stimme diesen zu</asp:HyperLink>
            </p>
            <p>
                <asp:Button ID="btAbschicken" runat="server" Text="Bestellung abschicken" OnClick="btAbschicken_Click" Visible="False" Width="170px" Font-Bold="True" />
            </p>
            <p>

            <p>
                &nbsp;
            </p>
        </div>
    </div>

</asp:Content>
