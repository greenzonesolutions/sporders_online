﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;


namespace SpordersOnline
{
    public partial class _Registrierungspflicht : Page
    {
        String restaurantname;
        string sfontcolor = "black";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)//nur beim ersten Laden
            {
                //Parameter = restaurantname auslesen
                restaurantname = Request.QueryString["restaurantname"];
                try
                {
                    //Bilder aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getPictureUrl.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string bildurl = data["bildurl0"].Value<string>();
                        string backgroundurl = data["backgroundurl0"].Value<string>();
                        sfontcolor = data["fontcolor0"].Value<string>();
                        Image1.ImageUrl = bildurl.TrimEnd('\r', '\n');//warum auch immer werden an das Ende des String Linefeeds hinzugefügt, die lösche ich hier wieder weg
                        divMasthead.Style["background-image"] = Page.ResolveUrl(backgroundurl);
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
                try
                {
                    //Leistungsmerkmale aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantLeistungsmerkmale.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string leistungsmerkmale = data["leistungsmerkmale0"].Value<string>();
                        if (leistungsmerkmale.Contains("liefern") || leistungsmerkmale.Contains("bestellen"))
                        {
                          
                        }
                        else
                        {
                            this.Master.FindControl("li_bestellen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservieren"))
                        {
                          
                        }
                        else
                        {
                            this.Master.FindControl("li_reservieren").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservierungsanfragen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservierungsanfragen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("registrierungspflicht"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_registrierungspflicht").Visible = false;
                        }
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
                lbHeadline.Text = "Willkommen bei " + restaurantname;

                uhrzeitInitialisieren();

                System.Drawing.Color fontcolor = System.Drawing.Color.Black;
                //change Color
                if (sfontcolor.Equals("white"))
                {
                    fontcolor = System.Drawing.Color.White;
                    text1.Style.Add("color", "white");
                }
                if (sfontcolor.Equals("black"))
                {
                    fontcolor = System.Drawing.Color.Black;
                    text1.Style.Add("color", "black");
                }
                lbHeadline.ForeColor = fontcolor;
               
            }
         }

        protected void uhrzeitInitialisieren()
        {
            List<string> kommen = new List<string>();
            List<string> gehen = new List<string>();

            Boolean ersterDurchlauf = true;
            kommen.Add("Uhrzeit Kommen"); //initialer Wert von ddUhrzeit damit man im Dropdown auch sieht für was das da ist
            gehen.Add("Uhrzeit Gehen"); //initialer Wert von ddUhrzeit damit man im Dropdown auch sieht für was das da ist
            for (int h = 8; h < 24; h++)
            {
                int m = 0;
                while (m < 60)
                {
                    if (ersterDurchlauf)
                    {
                        m = 0;
                        ersterDurchlauf = false;
                    }
                    if (m == 0)
                    {
                        kommen.Add(h + ":" + "00");
                        gehen.Add(h + ":" + "00");
                    }
                    else
                    {
                        kommen.Add(h + ":" + m);
                        gehen.Add(h + ":" + "00");
                    }
                    m = m + 15;
                }
            }
            ddKommen.DataSource = kommen;
            ddKommen.DataBind();

            ddGehen.DataSource = gehen;
            ddGehen.DataBind();
        }


        protected void btAbschicken_Click(object sender, EventArgs e)
        {
            try
            {
                string name = tbVorname.Text + " " + tbNachname.Text;
                string adresse = tbStraße.Text + " " + tbHausnummer.Text + " " + tbPlz.Text + " " + tbStadt.Text + " " + tbLand.Text;
                string telefonnummer = tbTelefonnummer.Text;
                string tischnummer = ddtischnummer.SelectedValue;
                string kommenzeit = ddKommen.SelectedValue;
                string gehenzeit = ddGehen.SelectedValue;
                string url = "http://greenzone.bplaced.net/db_registrieren.php?restaurant=" + Request.QueryString["restaurantname"] + "&name=" + name + "&telefonnummer=" + telefonnummer + "&adresse=" + adresse + "&tischnummer=" + tischnummer + "&kommenzeit=" + kommenzeit + "&gehenzeit=" + gehenzeit;
                if (tbVorname.Equals("") || tbStraße.Text.Equals(""))
                {
                    lbErrorMessage.Visible = true;
                }
                else
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd().Replace("\n", "");
                        Response.Redirect("Bestaetigung.aspx?restaurantname=" + Request.QueryString["restaurantname"] + "&ergebnis=" + responseFromServer);

                    }
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                lbErrorMessage.Visible = true;
            }

        }


    }

  
}