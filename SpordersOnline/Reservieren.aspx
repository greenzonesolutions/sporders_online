﻿<%@ Page Title="Reservieren" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reservieren.aspx.cs" Inherits="SpordersOnline._Reservieren" MaintainScrollPositionOnPostBack="true" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="DayPilot" namespace="DayPilot.Web.Ui" tagprefix="DayPilot" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" >
    <script type="text/javascript" src="../Scripts/modal.js"></script> 
    <script>
        function getdatetime(x)
        {
            var dateselect = new Date();
            dateselect = x.get_selectedDate();
            var timedisplay = new Date();
            x.get_element().value = formattedDate(dateselect);
        }
    </script>
    <script>
    function formattedDate(d) {
  let month = String(d.getMonth() + 1);
  let day = String(d.getDate());
  const year = String(d.getFullYear());

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return `${day}/${month}/${year}`;
    }
     </script>
    <div id="divMasthead" class="row" runat=server style="background-size: cover">
        <div class="col-md-4" style="color: #000000">
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="lbHeadline" runat="server" Font-Bold="True" Font-Size="XX-Large" ForeColor="Black" Text="Herzlich Willkommen bei "></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Image ID="Image1" runat="server" Width="100%" />
            </p>
            <br />
            <p>
                <asp:Label ID="lbReservieren" runat="server" Text="Wann möchten Sie reservieren?" Font-Bold="True" ForeColor="Black"></asp:Label>
            </p>
                            <asp:TextBox ID="tbDate" runat="server" placeholder="Datum" Width="175px" OnTextChanged="tbDate_TextChanged" AutoPostBack="true"></asp:TextBox>
              <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="tbDate" OnClientDateSelectionChanged="getdatetime" Format="dd/MM/yyyy"></cc1:CalendarExtender>
            &nbsp;<asp:DropDownList ID="ddRessourcen" runat="server" Width="175px" OnSelectedIndexChanged="ddRessourcen_SelectedIndexChanged" AutoPostBack="True" >
            </asp:DropDownList>
            <br />
            <br />
                <asp:Label ID="lbFreieZeit" runat="server" Text="Bitte wählen Sie Ihren Terminbeginn (Dauer und Ende planen wir für Sie ein):" Font-Bold="False" ForeColor="Black" Font-Size="Small" Visible="False"></asp:Label>
                            <DayPilot:DayPilotCalendar ID="DayPilotCalendar1" ClientObjectName="dp" runat="server" TimeFormat="Clock24Hours" DataEndField="end" DataStartField="start" DataTextField="name" DataIdField="id" DataResourceField="resource" TimeRangeSelectedHandling="PostBack" Width="100%" OnTimeRangeSelected="DayPilotCalendar1_TimeRangeSelected" style="left: 1px; top: 0px" Visible="False" EventResizeHandling="PostBack" OnEventResize="DayPilotCalendar1_EventResize" />
                <asp:TextBox ID="tbStart" runat="server" placeholder="Uhrzeit" Width="175px" Enabled="False" Visible="False"></asp:TextBox>
            &nbsp;<asp:Label ID="lbBis" runat="server" Text="-" Font-Bold="False" ForeColor="Black" Font-Size="Small" Visible="False"></asp:Label>
            &nbsp;
                <asp:TextBox ID="tbEnde" runat="server" placeholder="bis" Width="175px" Enabled="False" Visible="False"></asp:TextBox>
                            <br />
                <br />
                <asp:TextBox ID="tbTelefonnummer" runat="server" placeholder="Telefonnummer" Width="175px" Visible="False"></asp:TextBox>
                <br />
                <asp:TextBox ID="tbName" runat="server" placeholder="Name" Width="175px" Visible="False"></asp:TextBox>
                <br />
                            <asp:Label ID="lbErrorMessage" runat="server" ForeColor="#CC0000" Text="Bitte füllen Sie alle Daten vollständig aus" Visible="False"></asp:Label>
                <br />
            <asp:CheckBox ID="cbAGB" runat="server" Visible="False" />
            <asp:HyperLink ID="hlAGBs" Width="100%" runat="server" NavigateUrl="~/AGBs.aspx" Visible="False">Hiermit bestätige ich die AGBs und Datenschutzbestimmungen gelesen zu haben und stimme diesen zu</asp:HyperLink>
            <br />
                <asp:Button ID="btAbschicken" runat="server" Text="Reservierungsanfrage abschicken" OnClick="btAbschicken_Click" Font-Bold="True" Visible="False" />
            <br />
            <p style="color: #FFFFFF; font-weight: bold; font-size: large;">
                &nbsp;</p>
        </div>
    </div>
</asp:Content>
