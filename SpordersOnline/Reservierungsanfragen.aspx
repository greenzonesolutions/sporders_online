﻿<%@ Page Title="Reservierungsanfragen" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reservierungsanfragen.aspx.cs" Inherits="SpordersOnline._Reservierungsanfragen" MaintainScrollPositionOnPostBack="true" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" >
    <script>
        function getdatetime(x)
        {
            var dateselect = new Date();
            dateselect = x.get_selectedDate();
            var timedisplay = new Date();
            x.get_element().value = dateselect.format("dd/MM/yyyy");
        }
    </script>
     <div id="divMasthead" class="row" runat=server style="background-size: cover">
        <div class="col-md-4" style="color: #FFFFFF">
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="lbHeadline" runat="server" Font-Bold="True" Font-Size="XX-Large" ForeColor="Black" Text="Herzlich Willkommen bei "></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Image ID="Image1" runat="server" Width="100%" />
            </p>
            <br />
            <p>
                <asp:Label ID="Label11" runat="server" Text="Wann möchten Sie reservieren?" Font-Bold="True" ForeColor="Black"></asp:Label>
            </p>
                <table style="width: 61%">
                    <tr>
                        <td style="width: 194px; height: 30px;">
                            <asp:TextBox ID="tbDate" runat="server" placeholder="Datum" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width: 182px; height: 30px;">
                            <asp:DropDownList ID="ddUhrzeit" runat="server" Width="150px"></asp:DropDownList>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="tbDate" OnClientDateSelectionChanged="getdatetime"></cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 194px; height: 24px;">
                <asp:TextBox ID="tbTelefonnummer" runat="server" placeholder="Telefonnummer" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width: 182px; height: 24px;">
                <asp:DropDownList ID="ddAnzahl" runat="server" Width="150px">
                    <asp:ListItem>Anzahl Personen</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                </asp:DropDownList>
                        </td>
                    </tr>
                    </table>
                <br />
                            <asp:Label ID="lbErrorMessage" runat="server" ForeColor="#CC0000" Text="Bitte füllen Sie alle Daten vollständig aus" Visible="False"></asp:Label>
                <br />
            <asp:CheckBox ID="cbAGB" runat="server" />
            <asp:HyperLink ID="hlAGBs" Width="100%" runat="server" NavigateUrl="~/AGBs.aspx">Hiermit bestätige ich die AGBs und Datenschutzbestimmungen gelesen zu haben und stimme diesen zu</asp:HyperLink>
            <br />
                <asp:Button ID="btAbschicken" runat="server" Text="Reservierungsanfrage abschicken" OnClick="btAbschicken_Click" Font-Bold="True" />
            <br />
            <p style="color: #FFFFFF; font-weight: bold; font-size: large;">
                &nbsp;</p>
        </div>
    </div>

</asp:Content>
