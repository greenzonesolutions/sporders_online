﻿using DayPilot.Web.Ui.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;


namespace SpordersOnline
{
    
    public partial class _Reservieren : Page
    {
        String restaurantname;
        DataTable table;
        string sfontcolor = "black";
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)//nur beim ersten Laden
            {
                //Parameter = restaurantname auslesen
                restaurantname = Request.QueryString["restaurantname"];
                try
                {
                    //Bilder aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getPictureUrl.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        Image1.ImageUrl = responseFromServer.TrimEnd('\r', '\n');//warum auch immer werden an das Ende des String Linefeeds hinzugefügt, die lösche ich hier wieder weg
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }

                //Reservierungsressourcen auslesen
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantReservierungsRessourcen.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string ressourcen = data["reservierungsressourcen0"].Value<string>();
                        ressourcenInitialisieren(ressourcen);

                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
               
                try
                {
                    //Bilder aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getPictureUrl.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string bildurl = data["bildurl0"].Value<string>();
                        string backgroundurl = data["backgroundurl0"].Value<string>();
                        sfontcolor = data["fontcolor0"].Value<string>();
                        Image1.ImageUrl = bildurl.TrimEnd('\r', '\n');//warum auch immer werden an das Ende des String Linefeeds hinzugefügt, die lösche ich hier wieder weg
                        divMasthead.Style["background-image"] = Page.ResolveUrl(backgroundurl);
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
                try
                {
                    //Leistungsmerkmale aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantLeistungsmerkmale.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string leistungsmerkmale = data["leistungsmerkmale0"].Value<string>();
                        if (leistungsmerkmale.Contains("liefern") || leistungsmerkmale.Contains("bestellen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_bestellen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservieren"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservieren").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservierungsanfragen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservierungsanfragen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("registrierungspflicht"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_registrierungspflicht").Visible = false;
                        }
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }

                lbHeadline.Text = "Willkommen bei " + restaurantname;
                dayCalendarInitialisieren("");

                System.Drawing.Color fontcolor = System.Drawing.Color.Black;
                //change Color
                if (sfontcolor.Equals("white"))
                {
                    fontcolor = System.Drawing.Color.White;
                }
                if (sfontcolor.Equals("black"))
                {
                    fontcolor = System.Drawing.Color.Black;
                }
                lbHeadline.ForeColor = fontcolor;
                lbFreieZeit.ForeColor = fontcolor;
                lbBis.ForeColor = fontcolor;
                lbReservieren.ForeColor = fontcolor;
            }
        }

        protected void ressourcenInitialisieren(string ressourcenCSL)
        {
            string[] ressourcen;
            ressourcen = ressourcenCSL.Split(',');
            if(ressourcen.Length <= 1)
            {
                ddRessourcen.DataSource = ressourcen;
                ddRessourcen.Visible = false;
            }
            else
            {
                ressourcenCSL = "bei wem?," + ressourcenCSL;
                ressourcen = ressourcenCSL.Split(',');
                ddRessourcen.DataSource = ressourcen;
            }
            ddRessourcen.DataBind();
        }

        protected void dayCalendarInitialisieren(string ressource)
        {
            table = new DataTable();
            table.Columns.Add("id", typeof(string));
            table.Columns.Add("start", typeof(DateTime));
            table.Columns.Add("end", typeof(DateTime));
            table.Columns.Add("name", typeof(string));
            table.Columns.Add("resource", typeof(string));

            // DayPilotCalendar1.Resources.Add("", "1");
            using (var client = new WebClient())
            {
                string url = "";
                try
                {
                    if(ressource.Equals("bei wem?") || ressource.Equals(""))//ohne Ressource -> alle Zeitslots
                    {
                        url = string.Format("http://greenzone.bplaced.net/db_getBelegteZeitSlots.php?restaurant=" + Request.QueryString["restaurantname"]);
                    }
                    else
                    {
                        url = string.Format("http://greenzone.bplaced.net/db_getBelegteZeitSlots.php?restaurant=" + Request.QueryString["restaurantname"] + "&ressource=" + ressource);
                    }
                    var result = client.DownloadString(url);
                    JObject o = JObject.Parse(result);
                    List<string> belegteZeitSlots = new List<string>();
                    for (int i = 0; i < o.Count / 4; i++) //geteilt durch die Anzahl an Spalten in der DB
                    {
                        string belegterZeitSlot = (string)o["belegterzeitslot" + i.ToString()];
                        string[] anfangendZeit = belegterZeitSlot.Split(';');
                        DateTime startDate = DateTime.ParseExact(anfangendZeit[0], "yyyy-MM-dd HH.mm.ss", null);
                        DateTime endDate = DateTime.ParseExact(anfangendZeit[1], "yyyy-MM-dd HH.mm.ss", null);
                        table.Rows.Add("1", startDate, endDate, "belegt","");
                    }
                }

                catch (Exception e)
                {

                }
                DayPilotCalendar1.DataSource = table;
                DayPilotCalendar1.DataBind();
                Session.Add("DataTable", table);
            }

        }

        protected void btAbschicken_Click(object sender, EventArgs e)
        {
            try
            {
                //Start und Endzeit auf das Format yyyy-MM-dd HH.mm.ss
                CultureInfo MyCultureInfo = new CultureInfo("de-DE");
                DateTime startDate = DateTime.ParseExact(tbStart.Text, "dd.MM.yyyy HH:mm:ss", MyCultureInfo);
                DateTime endDate = DateTime.ParseExact(tbEnde.Text, "dd.MM.yyyy HH:mm:ss", MyCultureInfo);
               
                string startdatum = startDate.ToString("yyyy") + "-" + startDate.ToString("MM") + "-" + startDate.ToString("dd") + " " + startDate.ToString("HH") + ":" + startDate.ToString("mm") + ":" + startDate.ToString("ss");
                string enddatum = endDate.ToString("yyyy") + "-" + endDate.ToString("MM") + "-" + endDate.ToString("dd") + " " + endDate.ToString("HH") + ":" + endDate.ToString("mm") + ":" + endDate.ToString("ss");
                string url = "";
                if (ddRessourcen.SelectedValue.Equals("bei wem?") || ddRessourcen.SelectedValue.Equals(""))//ohne Ressource -> alle Zeitslots
                {
                    url = "http://greenzone.bplaced.net/db_reservieren.php?restaurant=" + Request.QueryString["restaurantname"] + "&anzahlgaeste=" + "dummy" + "&telefonnummer=" + tbTelefonnummer.Text + "&datum=" + startdatum + "&enddatum=" + enddatum + "&gastname=" + tbName.Text;
                }
                else
                {
                    url = "http://greenzone.bplaced.net/db_reservieren.php?restaurant=" + Request.QueryString["restaurantname"] + "&anzahlgaeste=" + "dummy" + "&telefonnummer=" + tbTelefonnummer.Text + "&datum=" + startdatum + "&enddatum=" + enddatum + "&gastname=" + tbName.Text + "&ressource=" + ddRessourcen.SelectedValue;
                }
                if (tbDate.Equals("Datum") || tbTelefonnummer.Text.Equals("Telefonnummer") || tbTelefonnummer.Text.Equals(" ") || tbStart.Text.Equals("von") || cbAGB.Checked == false){
                    lbErrorMessage.Visible = true;
                }
                else
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd().Replace("\n", "");
                        Response.Redirect("Bestaetigung.aspx?restaurantname=" + Request.QueryString["restaurantname"] + "&ergebnis=" + responseFromServer);

                    }
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                lbErrorMessage.Visible = true;
            }
        }

        protected void tbDate_TextChanged(object sender, EventArgs e)
        {
            DayPilotCalendar1.Visible = true;
            tbStart.Visible = true;
            //  tbEnde.Visible = true;
            //  lbBis.Visible = true;
            try
            {
                DateTime startDate = DateTime.ParseExact(tbDate.Text, "dd/MM/yyyy", null);
                DayPilotCalendar1.StartDate = startDate;
            }
            catch(Exception sdf)
            {

            }

            tbTelefonnummer.Visible = true;
            tbName.Visible = true;
            lbFreieZeit.Visible = true;
            cbAGB.Visible = true;
            hlAGBs.Visible = true;
            btAbschicken.Visible = true;
        }

        protected void DayPilotCalendar1_TimeRangeSelected(object sender, TimeRangeSelectedEventArgs e)
        {
            DataTable tableNeu = new DataTable();
            table = (DataTable)Session["DataTable"];
            tableNeu = table.Copy();
            tableNeu.Rows.Add("1", e.Start, e.End, "Ihre Auswahl", "");
            DayPilotCalendar1.DataSource = tableNeu;
            DayPilotCalendar1.DataBind();
            tbStart.Text = e.Start.ToString();
            tbEnde.Text = e.End.ToString();

        }

        protected void DayPilotCalendar1_EventResize(object sender, EventResizeEventArgs e)
        {
            DataTable tableNeu = new DataTable();
            table = (DataTable)Session["DataTable"];
            tableNeu = table.Copy();
            tableNeu.Rows.Add("1", e.NewStart, e.NewEnd, "Ihre Auswahl", "");
            DayPilotCalendar1.DataSource = tableNeu;
            DayPilotCalendar1.DataBind();
            tbStart.Text = e.NewStart.ToString();
            tbEnde.Text = e.NewEnd.ToString();
        }

        protected void ddRessourcen_SelectedIndexChanged(object sender, EventArgs e)
        {
            dayCalendarInitialisieren(ddRessourcen.SelectedValue);
        }
    }

  
}