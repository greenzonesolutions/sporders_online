﻿<%@ Page Title="Registrierungspflicht" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrierungspflicht.aspx.cs" Inherits="SpordersOnline._Registrierungspflicht" MaintainScrollPositionOnPostBack="true" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" >
    <script>
        function getdatetime(x)
        {
            var dateselect = new Date();
            dateselect = x.get_selectedDate();
            var timedisplay = new Date();
            x.get_element().value = dateselect.format("dd/MM/yyyy");
        }
    </script>
     <div id="divMasthead" class="row" runat=server style="background-size: cover">
        <div class="col-md-4" style="color: #FFFFFF">
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="lbHeadline" runat="server" Font-Bold="True" Font-Size="XX-Large" ForeColor="Black" Text="Herzlich Willkommen bei "></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Image ID="Image1" runat="server" Width="100%" />
            </p>
            <p class="MsoNormal">
                &nbsp;</p>
            <p class="MsoNormal">
                <span id="text1" runat="server">wir freuen uns, Sie als Gast begrüßen und bewirten zu dürfen. Nach § 2 Abs. 3 der CoronaVO Gast-stätten sind wir verpflichtet, zur Auskunftserteilung folgende Daten abzufragen.</span></p>
            <br />
            <p>
                <table style="width: 61%">
                    <tr>
                        <td style="width: 145px; height: 30px;">
                            <asp:TextBox ID="tbVorname" runat="server" placeholder="Vorname" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width: 194px; height: 30px;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 145px; height: 24px;">
                <asp:TextBox ID="tbNachname" runat="server" placeholder="Nachname" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width: 194px; height: 24px;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 145px; height: 24px;">
                <asp:TextBox ID="tbTelefonnummer" runat="server" placeholder="Telefonnummer" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width: 194px; height: 24px;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 145px; height: 24px;">
                        </td>
                        <td style="width: 194px; height: 24px;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 145px; height: 24px;">
                <asp:TextBox ID="tbStraße" runat="server" placeholder="Straße" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width: 194px; height: 24px;">
                <asp:TextBox ID="tbHausnummer" runat="server" placeholder="Hausnr" Width="60px"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
            </p>
                <table style="width: 61%">
                    <tr>
                        <td style="width: 21px; height: 30px;">
                <asp:TextBox ID="tbPlz" runat="server" placeholder="PLZ" Width="60px"></asp:TextBox>
                        </td>
                        <td style="width: 145px; height: 30px;">
                <asp:TextBox ID="tbStadt" runat="server" placeholder="Stadt" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
                <table style="width: 61%">
                    <tr>
                        <td style="width: 145px; height: 30px;">
                <asp:TextBox ID="tbLand" runat="server" placeholder="Land" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 145px; height: 30px;">
                            <asp:DropDownList ID="ddtischnummer" runat="server" Width="150px">
                                <asp:ListItem>Tischnummer</asp:ListItem>
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    </table>
                <table style="width: 61%">
                    <tr>
                        <td style="width: 24px; height: 30px;">
                            <asp:DropDownList ID="ddKommen" runat="server" Width="150px"></asp:DropDownList>
                        </td>
                        <td style="width: 145px; height: 30px;">
                            <asp:DropDownList ID="ddGehen" runat="server" Width="150px"></asp:DropDownList>
                        </td>
                    </tr>
                    </table>
            <br />
            <asp:HyperLink ID="hlAGBs" Width="100%" runat="server" NavigateUrl="~/Datenschutzhinweise.aspx">Datenschutz-Hinweise zur Erhebung personenbezogener Daten gemäß der CoronaVO Gaststätten</asp:HyperLink>
                <br />
                            <asp:Label ID="lbErrorMessage" runat="server" ForeColor="#CC0000" Text="Bitte füllen Sie alle Daten vollständig aus" Visible="False"></asp:Label>
                <br />
                <asp:Button ID="btAbschicken" runat="server" Text="Registrierung abschicken" OnClick="btAbschicken_Click" Font-Bold="True" />
            <br />
            <p style="color: #FFFFFF; font-weight: bold; font-size: large;">
                &nbsp;</p>
        </div>
    </div>

</asp:Content>
