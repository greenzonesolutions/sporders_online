﻿<%@ Page Title="Unternehmen auswählen" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FirmaWaehlen.aspx.cs" Inherits="SpordersOnline._FirmaWaehlen" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" >

    <div class="row">
        <div class="col-md-4" style="color: #FFFFFF">
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="lbHeadline" runat="server" Font-Bold="True" Font-Size="XX-Large" ForeColor="White" Text="Herzlich Willkommen bei Sporders "></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </p>
            <p>
                Bitte gewünschtes Partnerunternehmen aussuchen:
                <asp:DropDownList ID="ddCompanies" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddCompanies_SelectedIndexChanged">
                </asp:DropDownList>
            </p>
            <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
        </div>
    </div>

</asp:Content>
