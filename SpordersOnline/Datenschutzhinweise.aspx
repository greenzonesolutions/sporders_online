﻿<%@ Page Title="AGBs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Datenschutzhinweise.aspx.cs" Inherits="SpordersOnline._AGBs" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Datenschutz-Hinweise zur Erhebung personenbezogener Daten gemäß der CoronaVO Gaststätten<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Zu Zwecken der Auskunftserteilung gegenüber dem Gesundheitsamt oder der Ortspolizeibehörde erheben und speichern wir folgende Daten der Gäste:
•	Name und Vorname des Gastes,
•	Datum sowie Beginn und Ende des Besuchs, und
•	Telefonnummer oder Adresse des Gastes

Rechtsgrundlage hierfür ist Artikel 6 Absatz 1 Buchstabe c) der Datenschutz-Grundverordnung (DS-GVO) i.V.m. § 2 Absatz 3 Corona-Verordnung Gaststätten (Verordnung zur Eindämmung von Übertragungen des Corona-Virus (SARS-CoV-2) in Gaststätten vom 16. Mai 2020). 
Im Falle eines konkreten Infektionsverdachtes sind die zuständigen Gesundheitsbehörden oder Ortspolizeibehörden nach dem Bundesinfektionsschutzgesetz Empfänger dieser Daten.
Ihre personenbezogenen Daten werden von uns vier Wochen nach Erhalt gelöscht. 

Zur Angabe Ihrer persönlichen Daten sind Sie nicht verpflichtet; auch wird die Richtigkeit Ihrer Angaben vom Betreiber dieser Gaststätte nicht überprüft. Sollten Sie uns Ihre personenbezogenen Daten allerdings nicht zur Verfügung stellen, können Sie leider	unseren Betrieb derzeit nicht besuchen.
<o:p></o:p></span></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <b><span>Hinweis auf Betroffenenrechte:<o:p></o:p></span></b></p>
<p class="MsoNormal" style="color: #FFFFFF">
    <span>Sie haben nach der DS-GVO folgende Rechte: Auskunft über die personenbezogenen Daten, die wir von Ihnen verarbeiten; Berichtigung, wenn die Daten falsch sind oder Einschränkung unserer Verarbeitung; Löschung, sofern wir nicht mehr zur Speicherung verpflichtet sind.
Wenn Sie der Meinung sind, dass wir Ihre Daten nicht ordnungsgemäß verarbeiten, steht Ihnen außerdem ein Beschwerderecht beim Landesbeauftragten für den Datenschutz und die Informationsfreiheit Baden-Württemberg, Königstrasse 10a, Stuttgart zu.<o:p></o:p></span></p>
    <address style="color: #FFFFFF">
        &nbsp;</address>
</asp:Content>
