﻿<%@ Page Title="Speisekarte" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Speisekarte.aspx.cs" Inherits="SpordersOnline._Speisekarte" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent"  runat="server">
    <div id="divMasthead" class="row" runat=server style="background-size: cover">
        <div class="col-md-4" style="color: #FFFFFF">
            <p>
                &nbsp;
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                <asp:Label ID="lbHeadline" runat="server" Font-Bold="True" Font-Size="XX-Large" ForeColor="Black" Text="Herzlich Willkommen bei "></asp:Label>
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                <asp:Image ID="Image1" runat="server" Width="100%"/>
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                </p>
            <div id="divSpeisekarte" runat="server">
            </div>
        </div>
    </div>

</asp:Content>
