﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;


namespace SpordersOnline
{
    public partial class _Bestellen : Page
    {
        String restaurantname;
        string sfontcolor = "black";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)//nur beim ersten Laden
            {
                //Parameter = restaurantname auslesen
                restaurantname = Request.QueryString["restaurantname"];
                try
                {
                    lbHeadline.Text = "Willkommen bei " + restaurantname;
                    //Gerichte aus Datenbank zum passenden Restaurant holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantGerichte.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string gerichteJson = data["gerichte0"].Value<string>();
                        string[] gerichte = gerichteJson.Split('\n');
                        //ddGerichte.DataSource = gerichte;
                        //ddGerichte.DataBind();
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }

                try
                {
                    //Leistungsmerkmale aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantLeistungsmerkmale.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string leistungsmerkmale = data["leistungsmerkmale0"].Value<string>();
                        if (leistungsmerkmale.Contains("liefern"))
                        {
                            rbLiefern.Visible = true;
                        }
                        if (leistungsmerkmale.Contains("bestellen"))
                        {
                            rbBestellen.Visible = true;
                        }
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }

                try
                {
                    //Bilder aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getPictureUrl.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string bildurl = data["bildurl0"].Value<string>();
                        string backgroundurl = data["backgroundurl0"].Value<string>();
                        sfontcolor = data["fontcolor0"].Value<string>();
                        Image1.ImageUrl = bildurl.TrimEnd('\r', '\n');//warum auch immer werden an das Ende des String Linefeeds hinzugefügt, die lösche ich hier wieder weg
                        divMasthead.Style["background-image"] = Page.ResolveUrl(backgroundurl);
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
                try
                {
                    //Leistungsmerkmale aus Datenbank holen
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://greenzone.bplaced.net/db_getRestaurantLeistungsmerkmale.php?restaurant=" + restaurantname);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        Console.WriteLine(responseFromServer);
                        var data = (JObject)JsonConvert.DeserializeObject(responseFromServer);
                        string leistungsmerkmale = data["leistungsmerkmale0"].Value<string>();
                        if (leistungsmerkmale.Contains("liefern") || leistungsmerkmale.Contains("bestellen"))
                        {
                           
                        }
                        else
                        {
                            this.Master.FindControl("li_bestellen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservieren"))
                        {
                           
                        }
                        else
                        {
                            this.Master.FindControl("li_reservieren").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("reservierungsanfragen"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_reservierungsanfragen").Visible = false;
                        }
                        if (leistungsmerkmale.Contains("registrierungspflicht"))
                        {

                        }
                        else
                        {
                            this.Master.FindControl("li_registrierungspflicht").Visible = false;
                        }
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                }
                uhrzeitInitialisieren();
                gerichteInitialisieren();

                System.Drawing.Color fontcolor = System.Drawing.Color.Black;
                //change Color
                if (sfontcolor.Equals("white"))
                {
                    fontcolor = System.Drawing.Color.White;
                }
                if (sfontcolor.Equals("black"))
                {
                    fontcolor = System.Drawing.Color.Black;
                }
                lbHeadline.ForeColor = fontcolor;
                lbEuro.ForeColor = fontcolor;
                lbGesamtpreis.ForeColor = fontcolor;
                lbGesamtpreisText.ForeColor = fontcolor;
                lbWarenkorb.ForeColor = fontcolor;
            }
         }

        protected void rbAbholenLiefernChanged(object sender, EventArgs e)
        {
            if (rbLiefern.Checked)
            {
                trAdresse.Visible = true;
            }
            else
            {
                trAdresse.Visible = false;
            }
        }

        protected void btUebernehmen_Click(object sender, EventArgs e)
        {
            if (gvProdukte.SelectedRow != null)//soll nur ausgeführt werden wenn auch wirklich ein Produkt ausgewählt ist
            {
                lbWarenkorb.Visible = true;
                tbWarenkorb.Visible = true;
                btZuruecksetzen.Visible = true;
                btAbschicken.Visible = true;
                lbGesamtpreis.Visible = true;
                lbGesamtpreisText.Visible = true;
                lbEuro.Visible = true;
                hlAGBs.Visible = true;
                cbAGB.Visible = true;

                if (tbWarenkorb.Text.Length == 0)
                {
                    tbWarenkorb.Text = Server.HtmlDecode(tbWarenkorb.Text) + ddAnzahl.SelectedItem + "x " + Server.HtmlDecode(gvProdukte.SelectedRow.Cells[0].Text);
                    lbGesamtpreis.Text = String.Format("{0:0.00}", (Double.Parse(gvProdukte.SelectedRow.Cells[1].Text, CultureInfo.InvariantCulture) * Double.Parse(ddAnzahl.SelectedItem.Text, CultureInfo.InvariantCulture)));
                }
                else
                {
                    tbWarenkorb.Text = Server.HtmlDecode(tbWarenkorb.Text) + ", " + ddAnzahl.SelectedItem + "x " + Server.HtmlDecode(gvProdukte.SelectedRow.Cells[0].Text);
                    double eins = (Double.Parse(lbGesamtpreis.Text) + (Double.Parse(gvProdukte.SelectedRow.Cells[1].Text, CultureInfo.InvariantCulture) * Double.Parse(ddAnzahl.SelectedItem.Text)));
                    string zwei = String.Format("{0:0.00}", (Double.Parse(lbGesamtpreis.Text) + (Double.Parse(gvProdukte.SelectedRow.Cells[1].Text, CultureInfo.InvariantCulture) * Double.Parse(ddAnzahl.SelectedItem.Text))));
                    lbGesamtpreis.Text = String.Format("{0:0.00}",(Double.Parse(lbGesamtpreis.Text) + (Double.Parse(gvProdukte.SelectedRow.Cells[1].Text, CultureInfo.InvariantCulture) * Double.Parse(ddAnzahl.SelectedItem.Text))));
                }
            }
        }

        protected void btZuruecksetzen_Click(object sender, EventArgs e)
        {
            tbWarenkorb.Text = "";
        }

        protected void gerichteInitialisieren()
        {

            using (var client = new WebClient())
            {
                var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/db_getProdukte.php?restaurant=" + restaurantname));
                try
                {
                    JObject o = JObject.Parse(result);
                    dynamic dynJson = JsonConvert.DeserializeObject(result);

                    DataTable table = new DataTable();
                    DataColumn column;
                    DataRow row;
                    DataView view;

                    // Create first column.
                    column = new DataColumn();
                    column.DataType = System.Type.GetType("System.String");
                    column.ColumnName = "name";
                    table.Columns.Add(column);

                    // Create third column.
                    column = new DataColumn();
                    column.DataType = Type.GetType("System.String");
                    column.ColumnName = "preis";
                    table.Columns.Add(column);

                    for (int i = 0; i < o.Count / 2; i++) //geteilt durch die Anzahl an Spalten in der DB
                    {
                        row = table.NewRow();
                        row["name"] = (string)o["name" + i.ToString()];
                        row["preis"] = (string)o["preis" + i.ToString()];
                        table.Rows.Add(row);
                    }

                    // Create a DataView using the DataTable.
                    view = new DataView(table);
                    gvProdukte.Columns[0].ItemStyle.Width = Unit.Percentage(80);
                    gvProdukte.DataSource = view;
                    gvProdukte.DataBind();
                }
                catch(Exception e)
                {

                }
            }
        }

        protected void uhrzeitInitialisieren()
        {
            //Uhrzeit initialisieren
            DateTime centralDate = DateTime.Now;
            DateTime localDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(centralDate, "W. Europe Standard Time");
            int hour = localDate.Hour;
            int minute = localDate.Minute;

            if (minute < 15)
            {
                minute = 15;
            }
            else if (minute < 30)
            {
                minute = 15;
            }
            else if (minute < 45)
            {
                minute = 45;
            }
            else if (minute < 60)
            {
                minute = 0;
                hour++;
            }
            List<string> times = new List<string>();
            Boolean ersterDurchlauf = true;
            times.Add("Uhrzeit"); //initialer Wert von ddUhrzeit damit man im Dropdown auch sieht für was das da ist

            for (int h = hour; h < 24; h++)
            {
                int m = 0;
                while (m < 60)
                {
                    if (ersterDurchlauf)
                    {
                        m = minute;
                        ersterDurchlauf = false;
                    }
                    if (m == 0)
                    {
                        times.Add(h + ":" + "00");
                    }
                    else
                    {
                        times.Add(h + ":" + m);
                    }
                    m = m + 15;
                }

            }
            ddUhrzeit.DataSource = times;
            ddUhrzeit.DataBind();
        }

        protected void btAbschicken_Click(object sender, EventArgs e)
        {
            if (ddUhrzeit.SelectedValue.Equals("Uhrzeit") || tbTelefonnummer.Text.Equals("Telefonnummer") || (rbLiefern.Checked == true && tbAdresse.Text.Equals("")) || tbWarenkorb.Text.Equals("") || cbAGB.Checked == false) //Plausicheck
            {
                lbErrorMessage.Visible = true;
            }
            else
            {
                try
                {
                    string url = "";
                    //Gerichte aus Datenbank zum passenden Restaurant holen
                    if (rbLiefern.Checked)//liefern
                    {
                        url = "http://greenzone.bplaced.net/db_addLieferung.php?restaurant=" + Request.QueryString["restaurantname"] + "&gericht=" + tbWarenkorb.Text + "&bestellzeit=" + ddUhrzeit.SelectedItem + "&telefonnummer=" + tbTelefonnummer.Text + "&adresse=" + tbAdresse.Text;
                    }
                    else
                    {//bestellen
                        url = "http://greenzone.bplaced.net/db_addBestellung.php?restaurant=" + Request.QueryString["restaurantname"] + "&gericht=" + tbWarenkorb.Text + "&bestellzeit=" + ddUhrzeit.SelectedItem + "&telefonnummer=" + tbTelefonnummer.Text;
                    }
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd().Replace("\n", "");
                        Response.Redirect("Bestaetigung.aspx?restaurantname=" + Request.QueryString["restaurantname"] + "&ergebnis=" + responseFromServer);

                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                    lbErrorMessage.Visible = true;
                }
            }
        }

        protected void gvProdukte_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = gvProdukte.SelectedRow.Cells[0].Text;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvProdukte, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }
    }
}