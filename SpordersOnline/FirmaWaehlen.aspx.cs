﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;


namespace SpordersOnline
{
    public partial class _FirmaWaehlen : Page
    {
        String restaurantname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)//nur beim ersten Laden
            {
                using (var client = new WebClient())
                {
                    List<string> companies = new List<string>();
                    var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/db_getAllFirmen.php"));
                    try
                    {
                        JObject o = JObject.Parse(result);
                        dynamic dynJson = JsonConvert.DeserializeObject(result);
                        companies.Add("Bitte wählen");
                        for (int i = 0; i < o.Count; i++) //geteilt durch die Anzahl an Spalten in der DB
                        {
                            companies.Add((string)o["name" + i.ToString()]);
                        }

                        // Create a DataView using the DataTable.
                        ddCompanies.DataSource = companies;
                        ddCompanies.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
         }

        protected void ddCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx?restaurantname=" + ddCompanies.SelectedValue);
        }
    }
}