﻿<%@ Page Title="Startseite" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SpordersOnline._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent"  runat="server">
    <div id="divMasthead" class="row" runat=server style="background-size: cover">
        <div class="col-md-4" style="color: #FFFFFF">
            <p>
                &nbsp;
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                <asp:Label ID="lbHeadline" runat="server" Font-Bold="True" Font-Size="XX-Large" ForeColor="Black" Text="Herzlich Willkommen bei "></asp:Label>
            </p>
            <p>
                &nbsp;
            </p>
            <p>
                <asp:Image ID="Image1" runat="server" Width="100%"/>
            </p>
            <p>
                &nbsp;
            </p>
            <p id="pOeffnungszeiten" style="color: black; font-weight: bold; font-size: large;">
                <asp:Label ID="lbOeffnungszeitenHeader" runat="server" Text="Öffnungszeiten"></asp:Label>
            </p>
            <p>
                <asp:Label ID="lbOeffnungszeiten" runat="server" Text="Label" ForeColor="Black"></asp:Label>
            </p>
            <p id="pKontakt" style="font-size: large; font-weight: bold; color: black">
                <asp:Label ID="lbKontaktHeader" runat="server" Text="Kontakt:"></asp:Label>
            </p>
            <p>
                <asp:Label ID="lbKontakt" runat="server" Text="Label" ForeColor="Black"></asp:Label>
            </p>
            <p id="pAngebot" style="font-size: large; font-weight: bold; color: #000000;">
                <asp:Label ID="lbAngebotHeader" runat="server" Text="Angebot:"></asp:Label>
            </p>
            <p>
                <asp:Label ID="lbTagesangebot" runat="server" Text="Label" ForeColor="Black"></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <p>
                    <asp:Button ID="btBestellen" runat="server" Text="Ich möchte etwas bestellen" Width="280px" Height="38px" BackColor="Black" ForeColor="White" Font-Bold="True" Visible="False" OnClick="btBestellen_Click" />
            </p>
            <p>
                    <asp:Button ID="btReservieren" runat="server" Text="Ich möchte reservieren" Width="280px" Height="38px" BackColor="Black" ForeColor="White" Font-Bold="True" Visible="False" OnClick="btReservieren_Click" />
                    </p>
            <p>
                    <asp:Button ID="btReservierungsanfrage" runat="server" Text="Ich möchte reservieren" Width="280px" Height="38px" BackColor="Black" ForeColor="White" Font-Bold="True" Visible="False" OnClick="btReservierungsanfragen_Click" />
                    </p>
            <p>
                    <asp:Button ID="btSpeisekarte" runat="server" Text="Speisekarte ansehen" Width="280px" Height="38px" BackColor="Black" ForeColor="White" Font-Bold="True" Visible="False" OnClick="btSpeisekarte_Click" />
                    </p>
        </div>
    </div>

</asp:Content>
